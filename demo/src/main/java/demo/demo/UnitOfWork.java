package demo.demo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.sql.Statement;
import java.util.LinkedList;
import demo.demo.Entity;
import demo.demo.EntityState;
import java.util.HashMap;
import java.util.List;


public class UnitOfWork 
{

private Statement stat;
private Map<Entity,UnitOfWorkRepository> entities=new HashMap<Entity,UnitOfWorkRepository>();
private Connection conn;
{
try
{
conn.setAutoCommit(false);

} 
catch (SQLException e) 
{
e.printStackTrace();
	}
 }
@Override
public void Commit()
{

	for(Entity entity: entities.keySet())
	{
		switch(entity.getState())
		{
		
		case CHANGED :
			entities.get(entity).persistUpdate(entity);
			break;
		case DELETED:
			entities.get(entity).persistDelete(entity);
			break;
		case NEW:
			entities.get(entity).persistAdd(entity);
			break;
		case UNCHANGED;
			break;
		default:
			break;
			}
	}

try
{

	Connection connection;
	connection.commit();
	entities.clear();
}
 catch (SQLException e) 

 {
    e.printStackTrace();
 }
}


private void connsetAutoCommit(boolean b) 
{
	
}

public void markAsNew(Entity entity,UnitOfWorkRepository repo)
{
	entity.setState(EntityState.NEW);
	entities.put(entity, repo);
}

public void markAsDeleted(Entity entity,UnitOfWorkRepository repo)
{
	entity.setState(EntityState.DELETED);
	entities.put(entity, repo);
}

public void markAsDirty(Entity entity,UnitOfWorkRepository repo)
{
	entity.setState(EntityState.CHANGED);
	entities.put(entity, repo);
}

public void Rollback() 
{
	entities.clear();
}

}
