package demo.demo;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import demo.demo.UnitOfWorkRepository;
import demo.demo.Entity;
import demo.demo.EntityState;
import demo.demo.EnumerationValue;
import demo.demo.User;
 
import java.sql.SQLException;

public class HsqlEnumerationValuesRepository implements UnitOfWorkRepository ,EnumerationValueRepository
{

	Database db = new Database();
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	public void persistAdd(Entity entity) 
	{	
		 try {
			setUpInsertQuery((EnumerationValue)entity);
			insert.executeQuery();
	        } 
			catch (SQLException e)
	        {
	          
	         e.printStackTrace();
	        }
	}
		
	 private void setUpInsertQuery(EnumerationValue entity) 
	 {
		
	 }

	public void Delete(Entity entity)
	 {
		public void delete(EnumerationValue entity) 
		{
				entity.setState(EntityState.DELETED);
				db.enumValues.remove(entity);
		}
	 
	 public void Modify(Entity entity)
    {
		 entity.setState(EntityState.MODIFIED);
    }
	public void persistDelete(Entity entity) 
	{
		try
		{
			setUpInsertQuery((EnumerationValue)entity);
			insert.executeQuery();
	        } 
			catch (SQLException e)
	        { 
	         e.printStackTrace();
	        }
		
	}
	public void persistUpdate(Entity entity) 
	{
		try {
			setUpUpdateQuery((EnumerationValue)entity);
			update.executeUpdate();
			} 
			catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	private void setUpUpdateQuery(EnumerationValue entity)
	{	
	}

	public void withName(String name)
	{
		
	}

	public void withIntKey(int key, String name)
	{
		
	}

	public void withStringKey(String key, String name) 
	{	
	}

	public void Add(Object entity)
	{
	}

	public void Delete(Object entity) 
	{
		public void remove(EnumerationValue entity) {
					entity.setState(EntityState.DELETED);
					db.enumValues.remove(entity);	
	}
		
	public void Modify(Object entity) 
	{
		public void update(EnumerationValue entity) {
				entity.setState(EntityState.MODIFIED);
				db.enumValues.remove(entity);
	}
	
	public int Count() 
	{
		return db.enumValues.size();	
	}
	public Object withId(int id) 
	{
				
	}
	public  Object allOnPage(PagingInfo page)
	{
		public void add(EnumerationValue entity) {
					entity.setState(EntityState.NEW);
					db.enumValues.add(entity);
	}
}
