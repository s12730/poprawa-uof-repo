package demo.demo;

public interface RepositoryCatalog 
{

	
	public EnumerationValueRepository enumerations();
	public UserRepository users();

}