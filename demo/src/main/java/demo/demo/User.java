package demo.demo;

import java.util.List;

import domain.Person;
import domain.Role;

public class User  extends Entity
{
	private String login;
	private String password;
	public User(String login,String password) 
	{
		this.setLogin(login);
		this.setPassword(password);
}
	public String getLogin() 
	{
		return login;
	}
	public void setLogin(String login) 
	{
		this.login = login;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	private Person person;
 	private List<Role> roles;
}