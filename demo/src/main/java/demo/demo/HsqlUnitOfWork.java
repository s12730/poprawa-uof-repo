package demo.demo;
import demo.demo.Entity;
import demo.demo.EntityState;
import java.sql.Statement;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class HsqlUnitOfWork implements UnitOfWork
{

	private Map<Entity,UnitOfWorkRepository> entities;
	private Connection connection;
	public HsqlUnitOfWork(Connection connection) 
	{
		
		setEntities(new HashMap<Entity,UnitOfWorkRepository>());

}
	public Map<Entity,UnitOfWorkRepository> getEntities() 
	{
		return entities;
	}
	public void setEntities(Map<Entity,UnitOfWorkRepository> entities)
	{
		this.entities = entities;
	}
	public void saveChanges() 
	{
		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			
			case MODIFIED :
				entities.get(entity).persistUpdate(entity);
				break;
			case DELETED:
				entities.get(entity).persistRemove(entity);
				break;
			case NEW:
				entities.get(entity).persistAdd(entity);
				break;
			case UNCHANGED;
				break;
			case UNKNOWN:
				break;
			default:
				break;
				}
		}

	}
	
	public void markAsNew(Entity entity, UnitOfWorkRepository repo)
			{
		entity.setState(EntityState.NEW);
		entities.put(entity, repo);
	}
	
	public void markAsChanged(Entity entity, UnitOfWorkRepository repo)
	{
		entity.setState(EntityState.MODIFIED);
		entities.put(entity, repo);
	}
	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) 
	{
		entity.setState(EntityState.DELETED);
		entities.put(entity, repo);	
	}
	
	public void rollback()
	{
		entities.clear();
	}
}