package domain;


import java.util.StringTokenizer;
import demo.demo.Entity;
import java.util.ArrayList;
import java.sql.Date;

	public class Person extends Entity
	{
		private String firstName;
		private  String surname;
		private String pesel;
		private String nip;
		private String email;
		private Date dateOfBirth;
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getSurname() {
			return surname;
		}
		public void setSurname(String surname) {
			this.surname = surname;
		}
		public String getPesel() {
			return pesel;
		}
		public void setPesel(String pesel) {
			this.pesel = pesel;
		}
		public String getNip() {
			return nip;
		}
		public void setNip(String nip) {
			this.nip = nip;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Date getDateOfBirth() {
			return dateOfBirth;
		}
		public void setDateOfBirth(Date dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}
		 public void parse(String name) {
			    ArrayList<String> tokens = new ArrayList<String>();
			    StringTokenizer st = new StringTokenizer(name, " ");
			    while (st.hasMoreTokens()) {
			      String s = st.nextToken();
			      tokens.add(s);
			    }
			    if (tokens != null && !tokens.isEmpty()) {
			      this.firstName = tokens.get(0);
			      if (tokens.size() > 1)
			        this.surname = tokens.get(tokens.size() - 1);
			      if (tokens.size() > 2)
			        this.pesel = tokens.get(1);
			      if (tokens.size() > 3)
				    this.nip = tokens.get(2);
			      if (tokens.size() > 4)
					    this.email = tokens.get(3);
			      if (tokens.size() > 5)
					    this.dateOfBirth = tokens.get(4);
			    }
			  }

			  @Override
			  public String toString() {
			    StringBuffer sb = new StringBuffer();
			    sb.append(this.getFirstName());
			    sb.append(' ');
			    sb.append(this.getSurname());
			    sb.append(' ');
			    sb.append(this.getPesel());
			    sb.append(' ');
			    sb.append(this.getNip());
			    sb.append(' ');
			    sb.append(this.getEmail());
			    sb.append(' ');
			    sb.append(this.getDateOfBirth());
			    sb.append(' ');

			    return sb.toString();
			  }
			
}
